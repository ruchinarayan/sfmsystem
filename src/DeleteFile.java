import java.io.File;
import java.util.Scanner;

public class DeleteFile
{
	static void deleteFileFromSystem(String user) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		String fileToDelete;
		System.out.println("Enter the name of File to Delete :");
		fileToDelete = scn.nextLine();
		
		String filePath = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user+"\\"+fileToDelete;
		
		File delFile = new File(filePath);
		
		if(delFile.exists())
		{
			delFile.delete();
			System.out.println("File is deleted!!!");
		}
			else
		{
			System.out.println("File does'nt exist");
		}
		
	}
}