import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;



public class CheckFile
{
	private String algo;
	private File file,file1;
	String encPath;
	String decPath;
	public CheckFile(String algo,String path) {
		this.algo=algo; //setting algo
		this.file=new File(path); //setting file
	}

	public long encrypt() throws Exception{
		//opening streams
		FileInputStream fis =new FileInputStream(file);
		file=new File(file.getAbsolutePath()+".enc");
		FileOutputStream fos =new FileOutputStream(file);
		//generating key
		byte k[] = "HignDlPs".getBytes();   
		SecretKeySpec key = new SecretKeySpec(k,algo.split("/")[0]);  
		//creating and initialising cipher and cipher streams
		Cipher encrypt =  Cipher.getInstance(algo);  
		encrypt.init(Cipher.ENCRYPT_MODE, key);  
		CipherOutputStream cout=new CipherOutputStream(fos, encrypt);

		byte[] buf = new byte[1024];
		int read;
		while((read=fis.read(buf))!=-1)  //reading data
		cout.write(buf,0,read);  //writing encrypted data

		long fileSize = file.length();
		
		//System.out.println(fileSize);

		fis.close();
		cout.flush();
		cout.close();
		//System.out.println("File Encrypted !!!!");
		
		return fileSize;

	}
	static void checkFileFromSystem(String user) throws Exception {
		// TODO Auto-generated method stub

		String newFilePath;

		String userDir = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user;
		Scanner scn = new Scanner(System.in);
		File directory = new File(userDir);

		System.out.println("Enter the url of the file to Check :");
		newFilePath = scn.next();

		File extFile = new File(newFilePath);

		if(!extFile.exists() || !extFile.isFile())
		{
			System.out.println("Matching File does not Exist!!!");
		}
		else
		{
			//System.out.println("File length: "+extFile.length());
			//long fileSize = extFile.length();
			File[] fList = directory.listFiles();
			List<File> encList = new ArrayList<File>();
			for(File file: fList){
				if(file.toString().endsWith(".enc")){
					encList.add(file);
				}
			}
			CheckFile checkfile = new CheckFile("DES/ECB/PKCS5Padding",newFilePath);
			System.out.println("Following matched files as :");
			long encFileSize = checkfile.encrypt();
			for (File file : encList){
				long length = file.length();
				if (file.isFile() && length==encFileSize){
					System.out.println(file.getName());
				}
			}
		}	
	}
}