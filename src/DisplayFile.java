//import java.io.BufferedReader;
import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
import java.util.Scanner;

import decryption.fileDecryption;

//import encryption.fileEncryption2;

public class DisplayFile
{
	static void displayFileFromSystem(String user) throws Exception {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		String fileToDisplay;
		System.out.println("Enter the name of File to Display :");
		fileToDisplay = scn.nextLine();
		
		String filePath = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user+"\\"+fileToDisplay;
		
        File dispFile = new File(filePath);
		
		if(dispFile.exists())
		{
			new fileDecryption("DES/ECB/PKCS5Padding", filePath).decrypt();
		}
			else
		{
			System.out.println("File does'nt exist");
		}
		
	}
}