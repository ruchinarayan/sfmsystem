package decryption;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.SecretKeySpec;

public class fileDecryption
{
	  private String algo;
	  private File file;
	  FileReader fin = null;
	  BufferedReader bin = null;
	  
	  public fileDecryption(String algo,String path) {
	     this.algo=algo; //setting algo
	     this.file=new File(path); //setting file
	  }
	  
	  public void decrypt() throws Exception{
	         //opening streams
	         FileInputStream fis =new FileInputStream(file);
	         file=new File(file.getAbsolutePath()+".dec");
	         FileOutputStream fos =new FileOutputStream(file);               
	         //generating same key
	         byte k[] = "HignDlPs".getBytes();   
	         SecretKeySpec key = new SecretKeySpec(k,algo.split("/")[0]);  
	         //creating and initialising cipher and cipher streams
	         Cipher decrypt =  Cipher.getInstance(algo);  
	         decrypt.init(Cipher.DECRYPT_MODE, key);  
	         CipherInputStream cin=new CipherInputStream(fis, decrypt);
	               
	         byte[] buf = new byte[1024];
	         int read=0;
	         
	         
	         while((read=cin.read(buf))!=-1){  //reading encrypted data
	              fos.write(buf,0,read);  //writing decrypted data
	         //     for (int j = 0; j < buf.length; j++) {
	         //     System.out.print(((char)buf[j]));
	         //     }
	         }
	         //closing streams
	         cin.close();
	         fos.flush();
	         fos.close();
	         System.out.println("File Decrypted !!!!");
	        
	         System.out.println("Contents of file are :");
	 		
	 		try
	 		{
	 			fin = new FileReader(file);
	 			bin = new BufferedReader(fin);
	 			String s1 = bin.readLine();
	 			while(s1 != null)
	 			{
	 				System.out.println(s1);
	 				s1 = bin.readLine();
	 			}
	 		}
	 		catch(IOException e)
	 		{
	 			e.printStackTrace();
	 		}
	 		finally
	 		{
	 			try
	 			{
	 				if(bin != null)
	 				{
	 					bin.close();
	 					bin = null;
	 				}
	 			}
	 			catch(IOException e)
	 			{
	 				e.printStackTrace();
	 			}
	 			try
	 			{
	 				if(fin != null)
	 				{
	 					fin.close();
	 					fin = null;
	 				}
	 			}
	 			catch(IOException e)
	 			{
	 				e.printStackTrace();
	 			}
	 		}	         
	         
	     }
	  
	  
}