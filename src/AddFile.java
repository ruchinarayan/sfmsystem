import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import encryption.fileEncryption;
public class AddFile
{
	static void addFileToSystem(String user) throws Exception {
		// TODO Auto-generated method stub
		String fileToAdd,fileContent;
		Scanner scn = new Scanner(System.in);
		
		System.out.println("Enter the name of File to Add :");
		fileToAdd = scn.nextLine();
		System.out.println("Enter the content of File to Add :");
		fileContent = scn.nextLine();
		
		String filePath = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user+"\\"+fileToAdd;
		
		FileWriter out = null;
		BufferedWriter bout = null;
		
		try
		{
			out = new FileWriter(filePath);
			bout = new BufferedWriter(out);
			bout.write(fileContent);
			System.out.println("File named "+fileToAdd+" has been Saved !!");
			fileEncryption fen = new fileEncryption("DES/ECB/PKCS5Padding", "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user+"\\"+fileToAdd);
			fen.encrypt();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(bout!=null)
				{
					bout.flush();
					bout.close();
					bout = null;
				}
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			try
			{
				if(out!=null)
				{
					out.close();
					out=null;
				}
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
}