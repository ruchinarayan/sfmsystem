import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class ViewAvailUser
{
	static void viewAvailUserDetails() throws Exception
	{
		ViewUserFiles userFiles = new ViewUserFiles();
		ResultSet rs = null;
		String user;
		Scanner scn = new Scanner(System.in);
			String s2 = "select username from login";

			try
			{
				Class.forName("oracle.jdbc.driver.OracleDriver");
			}
			catch(ClassNotFoundException ex)
			{
				ex.printStackTrace();
			}
			Connection con = null;
			Statement stmt = null;
			try
			{
				con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","system");
				stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rs = stmt.executeQuery(s2);
				
				System.out.println("User's available :");
				while(rs.next())
				{
					System.out.println(rs.getString(1));
				}
				if(rs != null)
				{
					
					System.out.println("Enter the username you want to view files : ");
					user = scn.next();
					userFiles.displayAllAvailFiles(user);
				}
				else
				{
					System.out.println("No available User !!!!");
				}
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				try
				{
					if(stmt != null)
					{
						stmt.close();
					}
				}
				catch(SQLException ex)
				{
					ex.printStackTrace();
				}
				try
				{
					if(con != null)
					{
						con.close();
					}
				}
				catch(SQLException ex)
				{
					ex.printStackTrace();
				}
				try
				{
					if(rs != null)
					{
						rs.close();
					}
				}
				catch(SQLException ex)
				{
					ex.printStackTrace();
				}
			}
			
		
	}
}