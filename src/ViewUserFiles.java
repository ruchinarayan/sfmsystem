import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import decryption.fileDecryption;

public class ViewUserFiles
{
	public void displayAllAvailFiles(String user) throws Exception
	{
		String userDir = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user;
		Scanner scn = new Scanner(System.in);
		System.out.println("Following files are present for "+user+" : ");
		String displFile;
		File directory = new File(userDir);

		//get all the files from a directory

		File[] fList = directory.listFiles();
		List<File> encList = new ArrayList<File>();
		for(File file: fList){
			if(file.toString().endsWith(".enc")){
				encList.add(file);
				System.out.println(file.getName());
			}
		}
		
		System.out.println("Enter the File Name to display :");
		displFile = scn.next();

		String filePath = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user+"\\"+displFile;
		new fileDecryption("DES/ECB/PKCS5Padding", filePath).decrypt();
}
}