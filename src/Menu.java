import java.io.Console;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import encryption.PasswordEncryption;
public class Menu
{
	public void mainMenuDisplay(String user) throws Exception
	{
	   String fname,lname,encPass=null,pass=null;
		Scanner scn = new Scanner(System.in);
		DbUtil dbu = new DbUtil();
		Console cnsl = null;

	
	   while(true)
	   {
	   System.out.println("=====================================");
	   System.out.println("*** Select operations to perform ***");
	   System.out.println("=====================================");
	   System.out.println("1. To Add a New File (enter n/N).");
	   System.out.println("2. To Add an Existing File (enter e/E).");
	   System.out.println("3. To Display a File (enter d/D).");
	   System.out.println("4. To Delete a File (enter r/R).");
	   System.out.println("5. To Check a File (enter c/C).");
	   System.out.println("6. To Logout(enter l/L).");

	   if(user.equals("admin@memphis.edu"))
	   {
		   System.out.println("7. To create a New User (enter u/U).");
		   System.out.println("8. To view user's files (enter v/V).");
	   }
	   else
	   {
		   System.out.println("7. List down the files "+user+" has access to.(enter ua/UA)");
	   }
	   System.out.println("Enter your option :");
	   String inp = scn.nextLine();

	   if(inp.equals("d") || inp.equals("D"))
	   {
		   DisplayFile.displayFileFromSystem(user);
	   }
	   else if(inp.equals("e") || inp.equals("E"))
	   {
		   AddExistingFile.addExistingFileToSystem();
	   }
	   else if(inp.equals("n") || inp.equals("N"))
	   {
		   AddFile.addFileToSystem(user);
	   }
	   else  if(inp.equals("r") || inp.equals("R"))
	   {
		   DeleteFile.deleteFileFromSystem(user);
	   }
	   else  if(inp.equals("c") || inp.equals("C"))
	   {
		   CheckFile.checkFileFromSystem(user);
	   }
	   else  if(inp.equals("l") || inp.equals("L"))
	   {
          System.out.println("System logged out successfully !!!!");
		  break;
	   }
	   else  if((inp.equals("u") && user.equals("admin@memphis.edu")) || (inp.equals("U") && user.equals("admin@memphis.edu")))
	   {
		   System.out.println("=========================");
		   System.out.println("=== New User Creation ===");
		   System.out.println("=========================");
		   
		   System.out.print("Please enter First Name : ");
			fname = scn.next();
			System.out.print("Please enter Last Name : ");
			lname = scn.next();
			System.out.print("Please enter Username : ");
			user = scn.next();
			
			boolean same = false;

			do
			{
				
				try{
			         // creates a console object
			         cnsl = System.console();
			         
			         if (cnsl != null) {
			            

			        	// System.out.print("Please enter Password : ");
			        	 char[] pwd = cnsl.readPassword("Please enter Password : ");
							//pass = scn.next();
			        	 pass = new String(pwd);
						//	System.out.print("Re-enter Password : ");
							 char[] pwd1 = cnsl.readPassword("Re-enter Password : ");
							//pass1 = scn.next();
					     String pass1 = new String(pwd1);
							if(pass.equals(pass1))
							{
								same = true;
							}
							else
							{
								System.out.println("The entered passwords do not match. Please try again");
							}
							
							PasswordEncryption penc = new PasswordEncryption();
							encPass = penc.encryptPassword(pass);
							
							
			   
			         }      
			      }catch(Exception ex){
			         
			         // if any error occurs
			         ex.printStackTrace();      
			      }
				
				/*
				System.out.print("Please enter Password : ");
				pass = scn.next();
				System.out.print("Re-enter Password : ");
				pass1 = scn.next();
				if(pass.equals(pass1))
					same = true;
				else
					System.out.println("The entered passwords do not match. Please try again");
				*/
				
			}while(same == false);
	
			dbu.enterNewUser(fname, lname, user, pass, encPass);
			
			String userDir = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user;
			File newDir = new File(userDir);
			newDir.mkdir();
	   }
	   else  if(inp.equals("v") || inp.equals("V"))
	   {
          ViewAvailUser.viewAvailUserDetails();
	   }
	   else  if(inp.equals("ua") || inp.equals("UA"))
	   {
		   String userDir = "C:\\SampleProg\\SFMSystem\\SFMFilesStorage\\"+user;
		   File directory = new File(userDir);
		   System.out.println("Following Files as :");
		   File[] fList = directory.listFiles();
			List<File> encList = new ArrayList<File>();
			for(File file: fList){
				if(file.toString().endsWith(".enc")){
					encList.add(file);
					System.out.println(file.getName());
				}
			}
	   }
	   else
	   {
		   System.out.println("Please Enter a proper option!!");
	   }
	   
	   }
	   
	}
}