Steps to follow :

1. Install Java and Eclipse

2. Install Oracle 10g and SQL Developer

3. Create a table named login and insert 1 row for admin login :

create table login 
(firstname varchar(25),
lastname varchar(25),
username varchar(25),
password varchar(25),
encPass varchar(100)
);

insert into login values ('Admin','Admin','admin@memphis.edu','letmein','t6h1/B6iKLkGEEG3zsS9PFKrPOM=');

Note : Ideally we should not keep actual "password" column, but we added just to have a proof of the encrypted password column "encPass". 

5. Copy folder SFMSystem inside C:\SampleProg\

6. cd to C:\SampleProg\SFMSystem\ which has Executable jar file named SFMSystem.jar

7. Execute the jar file using following command :

java -jar SFMSystem.jar